
/*This module performs parameter learning over Hierachical Probabilic Logic Programs (HPLP)
using gradient descent and Backpropagation

@author Arnaud Nguembang Fadja
@copyright Arnaud Nguembang Fadja
 DPHIL: Deep Parameter learning for HIerarchical Probabilistic Logic programs
 Copyright (c) 2018, Arnaud Nguembang Fadja
*/

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>
#include <SWI-Prolog.h>
#include <unistd.h>
#include <sys/stat.h>
//#include <sys/types.h>
#define MaxName 10
#define ZERO 0.001
#define LOGZERO log(0.001)
// hyperparameters
#define RETURN_IF_FAIL if (ret!=TRUE) return ret;

typedef struct node_1 {
    char type[MaxName ]; // can be and,or,not,leaf                         
    double value; // node value.
    int index; // number of the rule for leaf node
    double gradient; // the derivative of the error with respect to the node
    struct node_1 *child;   // point to children of this node
    struct node_1 *next;    // point to next node at same level
}node;

void sigma_vec(double weights[],double probabilities[],int NR);
void initialize_weights_moments(double weights[],double Gradient[],double moments0[],double moments1[],int NR,double Max_W);
void print_Vector(double Gradient[],int num,char*VectorType);
node* Arithmetic_Circuit_to_tree(char *list);
node *create_AC();
void printAC(node* root,char*Choice);
void forward(double Probabilities[], int NR, node*root);
void backward(double Probabilities[], double Gradient[],int NR, node*root);
void update_weights_Adam(double Weights[], double Gradient[], int NR, double Moment_0[],double Moment_1[],int Iter,double Eta,double Beta1,double Beta2,long double Epsilon_adam_hat);
double forward_backward(node**Nodes,int lenNodes,int from,int to,double Weights[],double Gradients[],int NR,char *strategy);
double gradient_descent(node **Nodes,int lenNodes,int MaxIteration,double Weights[],int NR,double EA,double ER,double Eta,double Beta1,double Beta2,long double Epsilon_adam_hat,double Max_W,int BatchSize,char*strategy, char*datasetName,char* save);


// Utils
node * new_node(double val,char type[]){
    node *new_node = malloc(sizeof(node));

    if ( new_node ){
        new_node->next = NULL;
        new_node->child = NULL;
        new_node->gradient= 0.0;
        new_node->value = val;
        new_node->index =-1;
        strcpy(new_node->type,type);
    }
    return new_node;
}

node * add_sibling(node * n, double val,char type[]){
    if ( n == NULL )
        return NULL;

    while (n->next)
        n = n->next;

    return (n->next = new_node(val,type));
}

node * add_child(node * n, double val,char type[]){
    if ( n == NULL )
        return NULL;

    if ( n->child )
        return add_sibling(n->child, val,type);
    else
        return (n->child = new_node(val,type));
}

void construct_leaves_node(node **leaves_node,int lenRules){
  int i;
  for(i=0;i<lenRules;i++){
    leaves_node[i]=new_node(0.0,"leaf");
    leaves_node[i]->index=i;
  }
}
 //for saving and visualizing the learned weights and parameters
void openFiles (char * datasetName, FILE**probsFile, FILE** weightsFile,FILE** Moments0File,FILE** Moments1File,FILE** lls){
  struct stat st = {0};
  char nameFileProbs [40]="./";
  char nameFileWeights [40]="./";
  char nameFileClls [40]="./";
  char nameFileMoments0 [40]="./";
  char nameFileMoments1 [40]="./";
  strcat(datasetName, "_Values");
   // create the directory to save information
  if (stat(datasetName, &st) == -1) {
     mkdir(datasetName, 0700);
  }
  strcat(nameFileProbs,datasetName); strcat(nameFileProbs,"/probabilities.txt");
  strcat(nameFileWeights,datasetName); strcat(nameFileWeights,"/weights.txt");
  strcat(nameFileClls,datasetName); strcat(nameFileClls,"/clls.txt");
  strcat(nameFileMoments0,datasetName); strcat(nameFileMoments0,"/moments0.txt");
  strcat(nameFileMoments1,datasetName); strcat(nameFileMoments1,"/moments1.txt");

  *probsFile = fopen(nameFileProbs,"w");
  *weightsFile= fopen(nameFileWeights,"w");
  *lls=fopen(nameFileClls,"w");
  *Moments0File=fopen(nameFileMoments0,"w");
  *Moments1File=fopen(nameFileMoments1,"w");

  if(*probsFile==NULL || *weightsFile==NULL || *lls==NULL || *Moments0File==NULL || *Moments1File==NULL){
    printf("Problem in opening a file");
    exit(1);
  }
}

void closeFiles (FILE**probsFile, FILE** weightsFile,FILE** Moments0File,FILE** Moments1File,FILE** lls){
  fclose(*probsFile);
  fclose(*weightsFile);
  fclose(*Moments0File);
  fclose(*Moments1File);
  fclose(*lls);
}

void saveValues(double Probabilities[],double Weights [],double Moments0 [],double Moments1 [],int NR, FILE*probsFile, FILE* weightsFile,FILE* Moments0File,FILE* Moments1File,FILE* lls, double CLL){
  int i;
  for(i=0; i<NR; i++){
     fprintf(probsFile,"%f ",Probabilities[i]);
     fprintf(weightsFile,"%f ",Weights[i]);
     fprintf(Moments0File,"%f ",Moments0[i]);
     fprintf(Moments1File,"%f ",Moments1[i]);
  }
  fprintf(lls,"%f \n",CLL);
  fprintf(probsFile,"\n \n");
  fprintf(weightsFile,"\n \n");
  fprintf(Moments0File,"\n \n");
  fprintf(Moments1File,"\n \n");
}


void printAC(node* root,char*Choice){
  if(root!=NULL){
      node* n=root->child;
      if(strcmp(Choice,"values")==0){
          printf("%s(%lf) ",root->type,root->value);
      }else{
          if(strcmp(Choice,"gradients")==0){
             printf("%s(%lf) ",root->type,root->gradient);
          }
      }
      while(n!=NULL){
          printAC(n,Choice);
          n=n->next;
      }
  }else{
     perror("Null AC");
  }  
}

void sigma_vec(double Weights[],double Probabilities[],int NR){
  int i;
  for(i=0;i<NR;i++){
   Probabilities[i]=1/(1+exp(-Weights[i]));
  }
}
double randInRange(double min, double max)
{
  return min + (double)rand() / (RAND_MAX / (max - min + 1));
}
void initialize_weights_moments(double weights[],double Gradient[],double moments0[],double moments1[],int NR,double Max_W){
  int i;
  srand(time(NULL));
    for(i=0;i<NR;i++){
        weights[i]=randInRange(-Max_W,Max_W);
        
        //weights[i]=0.5;
        moments0[i]=0.0;
        moments1[i]=0.0;
        Gradient[i]=0.0;
    }
}
void print_Vector(double Vector[],int NR,char*VectorType){
 int i;
 char T[1];
 if(strcmp(VectorType,"gradient")==0){
   T[0]='G';
 }else{
    if(strcmp(VectorType,"weights")==0){
      T[0]='W';
    }else{
       if(strcmp(VectorType,"probabilities")==0){
         T[0]='P';
       } 
    }
 }
  printf("\n%s[",T);
  for(i=0;i<NR-1;i++){
   printf("%lf,",Vector[i]);
  }
  printf("%lf",Vector[i]);
  printf("]\n");
}


// Convert an arithmetic circuit (a term prolog) into n-aries tree in C
node *convertToAc(term_t AC,node **leaves_node,int lenRules){
  int i,j,ret,arity,ind;
  size_t lenNodes;
  atom_t name;
  char *type,*s;
  term_t current_term,current_List_term, temp_term;
  node *root,**nodes_ex;

  current_term=PL_new_term_ref();
  temp_term=PL_new_term_ref();
  current_List_term=PL_new_term_ref();
  if (!PL_is_integer(AC)) 
     ret=PL_get_compound_name_arity(AC,&name,&arity);
  else arity=0;

  if(arity!=0){ // non leaf node
      ret=PL_put_atom(temp_term,name);
      ret=PL_get_atom_chars(temp_term,&type);
      //RETURN_IF_FAIL
      root=new_node(0.0,type);
      
      for(i=1;i<=arity;i++){ // cycle over the argument: normally there are just one list argument for a particular term. Example: or[..]
       ret=PL_get_arg(i,AC,current_List_term);
       //RETURN_IF_FAIL
       if (PL_is_list(current_List_term))
       {
       ret=PL_skip_list(current_List_term,0,&lenNodes);
       //RETURN_IF_FAIL
       //if (ret!=PL_LIST) return PL_warning("argument List not found");
       if(lenNodes==1){ // The list has a single term. Example [2]
          ret=PL_get_list(current_List_term,current_term,current_List_term);
          //RETURN_IF_FAIL
          root->child=convertToAc(current_term,leaves_node,lenRules);
       }else{// The list has many terms
            nodes_ex=(node **)malloc(lenNodes*sizeof(node*));
            ret=PL_get_list(current_List_term,current_term,current_List_term);
            nodes_ex[0]=convertToAc(current_term,leaves_node,lenRules);
            root->child=nodes_ex[0];
            for(j=1;j<lenNodes;j++){
                ret=PL_get_list(current_List_term,current_term,current_List_term);
                nodes_ex[j]=convertToAc(current_term,leaves_node,lenRules);
                nodes_ex[j-1]->next=nodes_ex[j];
            }// end for            
       }// end else lenNodes==1
       }
       else
       root->child=convertToAc(current_List_term,leaves_node,lenRules);
     }//end cycle
  }else{ // arity=0 -> leaf node
    if(PL_is_integer(AC)){
     ret=PL_get_integer(AC,&ind);
     root=leaves_node[ind];
    }else{
     root=new_node(1.0,"leaf");
     root->index=-1;
    } 
  }// end arity!=0
  return root; 
}
/*
foreign_t pl_forward(term_t Circuit,term_t Parameters,term_t NR1,term_t Output){
  node * root, **leaves_node;
  char *name1;
  atom_t name;
  int NR,i,ret,arity;
  double *Probabilities;
  term_t ParamatersTerm, head,out, argument,temp_term;
  argument=PL_new_term_ref();
  temp_term=PL_new_term_ref();
  ret=PL_get_arg(1,Circuit, argument);
  ret=PL_get_compound_name_arity(argument,&name,&arity);
  ret=PL_put_atom(temp_term,name);
  ret=PL_get_atom_chars(temp_term,&name1);
  printf("Name=%s ",name1);
  if (strcmp(name1,"zero")==0){
      printf("Name=%s ",name1);
  }
 else {
    head=PL_new_term_ref();
    out=PL_new_term_ref();
    ret=PL_get_integer(NR1,&NR);
    leaves_node=(node **)malloc(NR*sizeof(node*));
    Probabilities=(double*)malloc(NR*sizeof(double));
    ParamatersTerm=PL_copy_term_ref(Parameters); 
    for(i=0;i<NR;i++){
      ret=PL_get_list(ParamatersTerm,head,ParamatersTerm);
      ret=PL_get_float(head,&(Probabilities[i]));
      printf("%lf ",Probabilities[i]);
    }
    root=convertToAc(Circuit,leaves_node,NR);
    forward(Probabilities,NR,root);

    // return the computed value
    ret=PL_put_float(out,root->value);
    ret=PL_unify(Output,out);
  }
  return ret;
}
*/


// Print hyperparameters
void  printHyperparams(double EA,double ER, int MaxIteration, double Eta,double Beta1,double Beta2, long double Adam_hat,double Max_W,int BatchSize,int lenNodes,char* strategy,char*datasetName,char*save){
  printf("\nHyperparameters of the dataset of the dataset %s: %d Arithmetic Circuits\n\n", datasetName,lenNodes);
  printf("Weights are initialized in the range [%.3lf %.3lf]\n",-Max_W,Max_W);
  printf("Max Iteration= %d \n",MaxIteration);
  printf("Epsilon= %lf \n",EA);
  printf("Delta= %lf \n",ER);
  printf("Eta= %lf \n",Eta);
  printf("Beta1= %lf \n",Beta1);
  printf("Beta2= %lf \n",Beta2);
  printf("Adam_hat= %.10Lf  \n",Adam_hat);
  printf("BatchSize= %d \n",BatchSize);
  printf("Strategy= %s \n",strategy);
  printf("DatasetName= %s \n",datasetName);
  printf("Save= %s \n\n",save);
}



foreign_t pl_gradient(term_t Nodes,term_t NR1,term_t EA1,term_t ER1,term_t MaxIter1,term_t BatchSize1,term_t Adam, term_t Stra_Name, term_t LL, term_t RulesProbabilities){ // 
  int ret,i,valid,count;
  term_t nodesTerm1,nodesTerm2,nodesTerm3,out1,out2,head,pterm;
  size_t lenNodes;
  int NR,MaxIter,BatchSize;
  char *strategy, *datasetName, *save;
  node * root,**leaves_node,**nodes_ex;
  double EA,ER,Eta,Beta1,Beta2,CLL,Adam_hat,Max_W;
  double *Weights,*Probabilities;
  head=PL_new_term_ref();
  out1=PL_new_term_ref();
  pterm=PL_new_term_ref();
  out2=PL_new_term_ref();

  //get the arguments
  nodesTerm1=PL_copy_term_ref(Nodes);
  nodesTerm2=PL_copy_term_ref(Adam); // nodesTerm2 contains a list of Adam arguments
  nodesTerm3=PL_copy_term_ref(Stra_Name); // nodesTerm2 contains the strategy and the name of the dataset
  ret=PL_get_integer(NR1,&NR);
  
  // get the thresholds
  ret=PL_get_float(EA1,&EA);
  ret=PL_get_float(ER1,&ER);

  // Get the maximun initial weights
  ret=PL_get_list(nodesTerm2,head,nodesTerm2);
  ret=PL_get_float(head,&Max_W);
  //Get adam parameters
  ret=PL_get_list(nodesTerm2,head,nodesTerm2);
  ret=PL_get_float(head,&Eta);
  ret=PL_get_list(nodesTerm2,head,nodesTerm2);
  ret=PL_get_float(head,&Beta1);
  ret=PL_get_list(nodesTerm2,head,nodesTerm2);
  ret=PL_get_float(head,&Beta2);
  ret=PL_get_list(nodesTerm2,head,nodesTerm2);
  ret=PL_get_float(head,&Adam_hat);
  
  // Get the maximun iteration, the learning strategy and the batch size
  ret=PL_get_integer(MaxIter1,&MaxIter);

  ret=PL_get_list(nodesTerm3,head,nodesTerm3);
  ret=PL_get_chars(head, &strategy,CVT_STRING);
  ret=PL_get_list(nodesTerm3,head,nodesTerm3);
  ret=PL_get_chars(head, &datasetName,CVT_STRING);
  ret=PL_get_list(nodesTerm3,head,nodesTerm3);
  ret=PL_get_chars(head, &save,CVT_STRING);

  ret=PL_get_integer(BatchSize1,&BatchSize);

  
  // Allocate space to store nodes, weights and probabilities
  ret=PL_skip_list(Nodes,0,&lenNodes);
  nodes_ex=(node **)malloc(lenNodes*sizeof(node*));
  leaves_node=(node **)malloc(NR*sizeof(node*));
  Weights=(double*)malloc(NR*sizeof(double));
  Probabilities=(double*)malloc(NR*sizeof(double));
  
  printHyperparams(EA,ER,MaxIter,Eta,Beta1,Beta2,Adam_hat,Max_W,BatchSize,lenNodes,strategy,datasetName,save);

  // Construct the leave nodes
  construct_leaves_node(leaves_node,NR);
  // construct the ACs: convert all the Acs into n-ary tress
  for(i=0;i<lenNodes;i++){
     ret=PL_get_list(nodesTerm1,head,nodesTerm1);
     root=convertToAc(head,leaves_node,NR);
     nodes_ex[i]=root;     
  }
  // Learning the parameters using the gradient descent algorithm.
  
  CLL=gradient_descent(nodes_ex,lenNodes,MaxIter,Weights,NR,EA,ER,Eta,Beta1,Beta2,Adam_hat,Max_W,BatchSize,strategy,datasetName,save); 
  // Return the last CLL and the learned probabilities. 
  ret=PL_put_float(out1,CLL);
  ret=PL_unify(LL,out1);
  sigma_vec(Weights,Probabilities,NR);

  ret=PL_put_nil(out2);
  for(i=NR-1;i>=0;i--){
     ret=PL_put_float(pterm,Probabilities[i]);
     ret=PL_cons_list(out2,pterm,out2);
  }
  ret=PL_unify(out2,RulesProbabilities);
  return ret;
}
  
double product_sum(node*nod,double Probabilities[],int NR){
   double prod_s=1;
   int index;
   node * n=nod;
   while (n!=NULL){
     if(strcmp(n->type,"leaf")==0){
        index=n->index;
        prod_s=prod_s*(1-Probabilities[index]);
     }else
     {
        prod_s=prod_s*(1-(n->value));
     }
     n = n->next;
  }
  return 1-prod_s;
}
double product(node*nod,double Probabilities[],int NR){
   double prod=1;
   int index;
   node * n=nod;
   while (n!=NULL){
     if(strcmp(n->type,"leaf")==0){
        index=n->index;
        prod=prod*Probabilities[index];
     }else
     {
        prod=prod*(n->value);
     }
     n = n->next;
  }
  return prod;
}

void forward(double Probabilities[], int NR, node*root){
  node * n;
  int index;
 if(root!=NULL){
    if(strcmp(root->type,"not")==0){
      forward(Probabilities,NR,root->child);
      root->value=1-(root->child)->value;
    }else{
        if(strcmp(root->type,"or")==0){
         // iterate on sibling
         n=root->child;
         while (n!=NULL){
           forward(Probabilities,NR,n);
           n = n->next;
         }
         root->value=product_sum(root->child,Probabilities,NR);
       }else{
          if(strcmp(root->type,"and")==0){
         // iterate on sibling
         n=root->child;
         while (n!=NULL){
           forward(Probabilities,NR,n);
           n = n->next;
        }
        root->value=product(root->child,Probabilities,NR);
       }else{ // leaf node;
           if(strcmp(root->type,"leaf")==0){
            index=root->index;
            root->value=Probabilities[index];
           }
         }
       }
     }
  }else{
   perror("Forward pass:NULL node");
 }  
}
void backward(double Probabilities[], double Gradient[],int NR, node*root){
 node * n;
 double Child_Gradient,Root_Gradient;
 
 if(root!=NULL){
  Root_Gradient=root->gradient;
  if(strcmp(root->type,"not")==0){
    n=root->child;
    while (n!=NULL){
      n->gradient=-Root_Gradient;
      backward(Probabilities,Gradient,NR,n);
      n = n->next;
    }
  }else{
      if(strcmp(root->type,"or")==0){
       // iterate over sibling
       n=root->child;
       while (n!=NULL){
         if(n->value!=0)
            Child_Gradient=Root_Gradient*(root->value/n->value);
         else
            Child_Gradient=Root_Gradient*(root->value/ZERO);
         n->gradient=Child_Gradient;
         backward(Probabilities,Gradient,NR,n);
         n = n->next;
       }
     }else{
       if(strcmp(root->type,"and")==0){
         // iterate on sibling
         n=root->child;
         while (n!=NULL){       
           if(strcmp(n->type,"leaf")==0){
             Child_Gradient=Root_Gradient*(root->value)*(1-(n->value));
             n->gradient=Child_Gradient;
             backward(Probabilities,Gradient,NR,n); // propagate the parent value
           }else{
              if(1-(n->value)!=0)
                Child_Gradient=Root_Gradient*((1-(root->value))/1-(n->value));
              else
                Child_Gradient=Root_Gradient*((1-(root->value))/ZERO);
              n->gradient=Child_Gradient;
              backward(Probabilities,Gradient,NR,n);
           }
           n = n->next;
         }
     }else{
           if(strcmp(root->type,"leaf")==0){
              int index=root->index;
              Gradient[index]=Gradient[index]+ root->gradient;
           }
     }
   }
 }
 }else{
    perror("Backward pass:NULL node");
  }
}

void update_weights_Adam(double Weights[], double Gradients[], int NR, double Moment_0[],double Moment_1[],int Iter,double Eta,double Beta1,double Beta2,long double Epsilon_adam_hat){
  int Iter_new,i;
  double Result1,Result2,Eta_new;
  Iter_new =Iter+1;
  Result1=sqrt(1-pow(Beta2,Iter_new));
  if((1-pow(Beta1,Iter_new))!=0)
    Eta_new=Eta*Result1/(1-pow(Beta1,Iter_new));
  else
    Eta_new=Eta;
  for(i=0;i<NR;i++){
     Moment_0[i]=Beta1*Moment_0[i] + (1-Beta1)*Gradients[i];
     Moment_1[i]=Beta2*Moment_1[i] + (1-Beta2)*Gradients[i]*Gradients[i];
     Result2=sqrt(Moment_1[i]);
     Weights[i]=Weights[i]-Eta_new*Moment_0[i]/(Result2+Epsilon_adam_hat);
     // reinitialize the gradient after updating
     Gradients[i]=0.0;
  }
}
  // This function computes the range index of the next batch 
void nextBatch(int*from,int*to,int lenNodes,int BatchSize,char*strategy){
  int from1=*from,to1=*to;
  if(strcmp(strategy,"minibatch")==0){
    if(from1+BatchSize>=lenNodes){
      from1=0;to1=BatchSize;
    }else{
      if(to1+BatchSize>=lenNodes){
        from1=to1;to1=lenNodes;
      }else{
        from1+=BatchSize;to1+=BatchSize;
      }
    }
  }else{//end minibatch
    if(strcmp(strategy,"stochastic")==0){
        from1=0; to1=BatchSize;
    }else{
      if(strcmp(strategy,"batch")==0){
        from1=0; to1=lenNodes;
      }else{
        perror("strategy not found");
      }
    }
  }
*from=from1; *to=to1;
//return ;
}
void normalize_grad(double Gradients[],int NR,int Num_nodes){
  int i;
  for(i=0;i<NR;i++){
    Gradients[i]=Gradients[i]/Num_nodes;
  }
}
double forward_backward(node**Nodes,int lenNodes,int from,int to,double Weights[],double Gradients[],int NR,char *strategy){
  double Root_Value;
  double Probabilities[NR],CLL=0;
  int i,index;
  srand(time(NULL)); // for the stochastic strategy
  sigma_vec(Weights,Probabilities,NR);
  //printf("from %d to %d",from,to);
  for(i=from;i<to;i++){
     if(strcmp(strategy,"stochastic")==0){
        index=rand()%lenNodes; // generate a number between 0 and lenNodes-1
     }else{
       index=i;
     }
     //printf("\nIndex= %d",index);
    forward(Probabilities,NR,Nodes[index]);
    Root_Value=Nodes[index]->value;
    if(Root_Value!=0){
      CLL=CLL+log(Root_Value);
      Nodes[index]->gradient=-1/(Root_Value);
    }else{
      CLL=CLL+LOGZERO;
      Nodes[index]->gradient=-1/ZERO; 
    }  
    // backward pass
    backward(Probabilities,Gradients,NR,Nodes[index]);
  }
  return CLL;
}

double gradient_descent(node **Nodes,int lenNodes,int MaxIteration,double Weights[],int NR,double EA,double ER,double Eta,double Beta1,double Beta2,long double Epsilon_adam_hat,double Max_W,int BatchSize,char*strategy,char* datasetName, char* save){
  double Gradients[NR],Moments0[NR],Moments1[NR],Probabilities[NR];
  int from,to,Iter,saved=0;
  double CLL0= -2.2*pow(10,10); //-inf
  double CLL1= -1.7*pow(10,8);  //+inf
  double ratio,diff,MaxIteration1=MaxIteration;
  FILE*probsFile,*weightsFile, *lls, *Moments0File, *Moments1File; 
  
  if(BatchSize >lenNodes){ // if the batch size is great than the training set it to the cardinality of the training set
      BatchSize=lenNodes;
  }
  
  diff=fabs(CLL1-CLL0);
  ratio=diff/fabs(CLL0);
  if (MaxIteration==-1)
    MaxIteration1= 2147000000;
  Iter=0;
  sigma_vec(Weights,Probabilities,NR);
  if(strcmp(save,"Yes")==0 || strcmp(save,"yes")==0 || strcmp(save,"YES")==0 || strcmp(save,"YeS")==0)
     saved=1;
  initialize_weights_moments(Weights,Gradients,Moments0,Moments1,NR,Max_W);
  if(saved==1){
      openFiles (datasetName,&probsFile, &weightsFile,&Moments0File,&Moments1File,&lls); 
      saveValues(Probabilities,Weights,Moments0,Moments1,NR,probsFile,weightsFile,Moments0File,Moments1File,lls,CLL1);
  }
  from=0; to=BatchSize;
  while(Iter<MaxIteration1 && diff>EA && ratio>ER){ //   && diff>EA && ratio>ER
      CLL0 = CLL1;
      CLL1=forward_backward(Nodes,lenNodes,from,to,Weights,Gradients,NR,strategy);
      normalize_grad(Gradients,NR,to-from+1);
      diff=fabs(CLL1-CLL0);
      ratio=diff/fabs(CLL0);
      update_weights_Adam(Weights,Gradients,NR,Moments0,Moments1,Iter,Eta,Beta1,Beta2,Epsilon_adam_hat);
      sigma_vec(Weights,Probabilities,NR);
      if(saved==1)
        saveValues(Probabilities,Weights,Moments0,Moments1,NR,probsFile,weightsFile,Moments0File,Moments1File,lls,CLL1);
      Iter++;
      nextBatch(&from,&to,lenNodes,BatchSize,strategy);
  }//end while
  if(saved==1)
    closeFiles (&probsFile, &weightsFile,&Moments0File,&Moments1File,&lls);
  return CLL1;
}
install_t
install()
{ 
  PL_register_foreign("gradient",10, pl_gradient, 0);
  //PL_register_foreign("forward_C",4, pl_forward, 0);
}